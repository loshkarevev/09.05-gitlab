FROM centos:7
RUN yum update -y && yum install -y 'python3.9' python3-pip
RUN yum -y install gcc openssl-devel bzip2-devel libffi-devel zlib-devel && yum install -y wget && yum install -y make
RUN wget https://www.python.org/ftp/python/3.9.1/Python-3.9.1.tgz
RUN tar xzf Python-3.9.1.tgz 
RUN cd Python-3.9.1 && ./configure --enable-optimizations && make altinstall
RUN python3.9 -V
RUN pip3 install flask flask-jsonpify flask-restful

ADD app.py /python-api/
ENTRYPOINT | "python3", "/python-api/app.py" |
